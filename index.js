//npm install express mongoose cors
//the packages:
/*"dependencies": {
    "cors": "^2.8.5",
    "express": "^4.18.1",
    "mongoose": "^6.6.5"
  }*/
  //check the "notes" file, we added "start": index.js to the package.
  //this will be like -nodemon index.js- but this time we will use -nmp start-
//////////////////////////////////////////////////////////////////////////////
//1st load the express
// const express = require("express");
// const mongoose = require("mongoose")
// const cors = require("cors")
// const port = 4000;
// const app = express();
// app.use = express();
// app.listen(port, () => {
// 	console.log(`API is now online on port ${port}`);
// })
//////////////////////////////////////////////////////////////////////////////

/////////////////////////
//1st load the express///
/////////////////////////
const express = require("express");;
const mongoose = require("mongoose");  //line 33-34 confirmed this on terminal that it is workiing.
const cors = require("cors")
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
//11111111111111111111111111111111111111111111111111111111111111111
// const archiveRoutes = require("./routes/archiveRoutes")
//11111111111111111111111111111111111111111111111111111111111111111
const port = process.env.PORT || 4000;
const app = express();
//=================================================================
//connect to our MongoDB
mongoose.connect("mongodb+srv://admin:admin123@batch204.nanl81q.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,											  //hard coded
	useUnifiedTopology: true 										  //hard coded
});
let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));			  //to verify on terminal that we are connected to Mongo DB Atlas
db.once("open", () => console.log('Now connected to MongoDB Atlas!')) //to verify on terminal that we are connected to Mongo DB Atlas
//=================================================================
app.use(cors());		//this means we will use cors require line 25
app.use(express.json());	//this means we will use express require line 24
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
//11111111111111111111111111111111111111111111111111111111111111111
// app.use("/archive", archiveRoutes);
//11111111111111111111111111111111111111111111111111111111111111111
//=================================================================
app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});

//////////////////////////////////////////////////////////////////////////////
//A JWT is an object that securely transmits information between parties in a compact and
//self contained way.
//transmitted info can be verified and trusted because a JWT is trusted
//JWT token, is like a ticket, authenticating you if you have access to something and to what extent you have access to.
//example if you can access as an admin or just a regular user/



//JWS or jasonwebtoken that will be issued to us if we are verified to the db.
//User Registration, process of creating a new or adding a user to the document in the users
    //collection once all REQUIRED information has been received whic you will see from the SCHEMA/MODEL.

//User Authentication, process of verifying that a user's IDENTITY is true
    //usually via a username and password combination.
    //once user identity has been proven, a JWT may be used to the sourse of the request.


