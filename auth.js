const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";
//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => { 
	//console.log(result);  			//this is the result of findONE. which is an object about the doc from the collection
	const data = { 					//this result is the same result in JWT if you will get the token from POSTMAN.
		id: user._id, 				//we used dot notation here.
		email: user.email,
		isAdmin: user.isAdmin		//const data includes only the things that we want to see.
	}
	//.sign(<payload/const data>, <const secret is a user defined>, <option for the expiration of the token >)
	return jwt.sign(data, secret, {}) //sign method to generate the token from JWT that you will see sa POSTMAN.
}// return jwt.sign(data, secret, (expiresIn: "2h" ))  hours of expiration should be in Sring e.g "5h"

//s38
//TOken Verification  -this time we will not use "req.headers.authorization" but req.headers as part of our request object
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	/*	This is the what we get from request headers, else undefined if wala tayong na provide na token sa auth, type-bearer token, token place holder ang pasted the JWT toke, once we send a request this is what we should see sa TERMINAL.
		Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxNTViNTM3YmRlNGYxNDRmYTMxYyIsImVtYWlsIjoibGV4dXMxMjNAbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjY1NDA0NzgyfQ.FCFrjupETuPLKVtJbycKe0gY705jo12OG8TU5NZlIsk
	*/
	if(typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length); //means from 1st to 6th character will be removed, from 7th onwards will remain. 
		return jwt.verify(token, secret, (err, data) => {	//to validate out token if legit, we slice token because of verify,
			if(err) {
				return res.send({auth: "failed"})
			} else {
				next();
			}
		}) 
	
	} else {
		return res.send({auth: "failed"});
	}
}	//go to POSTMAN, auth/bearerToken and paste the JWT token later.

//s38
//Token Decryption	-for us to get the payload data from JWT.
module.exports.decode = (token) => {
	if(typeof token !== "undefined") { 		//if si token daw hindi undefined, ito yung sa postman auth/bear token/token.
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload //out of 3 parts of token in JWT heade/payload/verify signature, here we just needed the payload.
			}											//you may skip .payload, you may also use .header, .signature and even .id just to get the id. 
		})

	} else {
		return null
	}
}