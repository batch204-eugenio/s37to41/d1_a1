const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth.js");
//-------------------------------------------------------------------------
//Route for creating a course.
router.post("/", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin
	console.log(isAdmin)
	if(isAdmin) {
		courseController.addCourse(req.body).then(resultFromController =>
		res.send(resultFromController));
	} else {
		res.send(false);
	}
});
//s39 ROute for retrieving all the resources
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

//s39 Route for retrieving all the active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//s39 Route for retrieving specific course
router.get("/:courseId", (req, res) => { 													//so sa url kunin natin ang ID number e.g. 6345408c492ff293d6c7baa3 tapos idudugtong natin sya sa url, e.g. localhost:4000/courses/6345408c492ff293d6c7baa3
	console.log(req.params.courseId)  														 //we just added 6345408c492ff293d6c7baa3 then press send on the POSTMAN since we are just retrieving.
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

//s39 Route for Updating a course
router.put("/:courseId", auth.verify, (req, res) => { //sa POSTMAN gayahin lang natin si line 27 pero wag muna mag send kasi may req.params, req.body, userData tayo na iprovide.
	const userData = auth.decode(req.headers.authorization)			//bale una nag log in muna tayo sa LOGIN ng username at password ng user na nasa mongoDB kasi meron tayong auth.very at para iverify if kung TRUE na admin tayo as you will see sa mongoDB dapat naka TRUE, tapos if true, press send, true dapat result sa POSTMAN tapos bibigyan ka ng token, pag makakuha na tayo ng token, punta tayo sa UPDATE COURSE, kunin mo muna id ni user at idugtong sa URL, before mag send i-update natin sa UPDATE COURSE si body,raw,json ang value ng "name", "description", "price", tapos  
	courseController.updateCourse(req.params, req.body, userData).then(resultFromController => //tapos dahil may auth.verify nga tayo sa UPDATE COURSE pa din punta ka auth/type/bearer token/ Token then paste/ dapat true pa din ang ilalabas, pag true check the m0ngoDB dapat na update na dun.
		res.send(resultFromController));

});

//-------------------------------------------------------------------------
module.exports = router;































////activity/// kunin si course admin to limit the course creation
/*
	Mini-Activity:
		Limit the Course creation to admin only. Refactor the course route/controller.
*/
//39activity start
// const express = require("express");
// const router = express.Router();
// const courseController = require("../controllers/courseController");
// const auth = require("../auth.js");
// //39start==================
// const user = require("../models/User");
// //39end====================
// //-------------------------------------------------------------------------
// //Route for creating a course.
// //-------------------------------------------------------------------------

// router.post("/", auth.verify, (req, res) => {
// if (isAdmin !== true) {
// 	courseController.addCourse(req.body).then(resultFromController =>
// 		res.send(resultFromController));
// 	} else {
// 		return "User is not an Admin";
// 	}
// })
// module.exports = router;