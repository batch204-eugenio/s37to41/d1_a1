const express = require("express");		
const router = express.Router();
const userController = require("../controllers/userController")
//s38start
const auth = require("../auth");
//s38end
//======================================================================
//Route for checking if user's email already exists on the db  
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//======================================================================
//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//======================================================================
//Route for User Authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});
//s38 start
router.post("/details", auth.verify, (req, res) => {	//auth.verify serves as a middleware, w/c means before we can send a request, it will check first if the user is logged in.
		const userData = auth.decode(req.headers.authorization);
		//  This is what the laman ni payload kasama yung token na naka slice galing sa auth/bear token/token.
		// {
		//   id: '6340155b537bde4f144fa31c',
		//   email: 'lexus123@mail.com',
		//   isAdmin: false,
		//   iat: 1665404782
		// }

// 		
		console.log(userData);
		console.log(req.headers.authorization);
		userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))
});
// //s38 end
// /////////////////////////////////////////////////////////////////////////////////////
// ///ACTIVITY-START////
// 	// router.post("/details", (req, res) => {
// 	// 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
// 	// });
// //ACTIVITY-END///
// //ZUITT ANSWER//
// 	// router.post("/details", (req, res) => {
// 	// 	userController.getProfile({id: req.body.id}).then(resultFromController => res.send(resultFromController))
// 	// });
// //ZUITT END//
// /////////////////////////////////////////////////////////////////////////////////////
module.exports = router;				
// //MIAH END//
