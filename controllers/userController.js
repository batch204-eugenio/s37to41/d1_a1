const User = require("../models/User");	
const bcrypt = require("bcrypt");	
const auth = require("../auth");					//double dot kasi folder tapos subfolder.
//=========================================================
//Controller to Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result  => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// //=========================================================
//Controller for User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataTobeHas>, <saltRound>) //salt round e.g. 10 means 10x, this means the safest.
		password: bcrypt.hashSync(reqBody.password, 10) //you will see on mongoBD that the password has been hashed, you can hash everything not just password.
	})
	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}
//=========================================================
//Controller for User Authentication
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            //bcrypt.compareSync,(<dataToBeCompared that is not hashed>,<dataFromDB that is encrypted or hashed>) 
            if(isPasswordCorrect) {   
            	console.log(result); //this is the result from FINDONE>             
                return {access: auth.createAccessToken(result)} //access is from POSTMAN for JWT but this is user defined.
            } else {											//
                return false
            }
        }
    })
}
//this reference to above .loginUser is from postman the JWT token.
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNDAxNTViNTM3YmRlNGYxNDRmYTMxYyIsImVtYWlsIjoibGV4dXMxMjNAbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjY1MTQ2NTA3fQ.KjFdQWNf-YlUbdD7x2aUJl3ZvJ5NGqHZTer45iGvrv4

/////////////////////////////////////////////////////////////////////////////////////
//ACTIVITY-START//
//we will not use .save here since we will not use it to our data base.
	module.exports.getProfile = (reqBody) => {
		return User.findById(reqBody.id).then(result => {
			result.password = "";
			return result
		})
	}
//ACTIVITY-END//
//ZUITT ANSWER//
	// module.exports.getProfile = (reqbody) => {
	// 	return User.findById(data.Id).then(result => {
	// 		result.password = "";
	// 		return result
	// 	})
	// }
//ZUITT END//
/////////////////////////////////////////////////////////////////////////////////////