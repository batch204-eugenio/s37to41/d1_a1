const Course = require("../models/Course");
const User = require("../models/User"); //40
//Create a New Course
module.exports.addCourse = (reqBody) => { 		//.addCourse is a function name, 
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	//let us save this courses that we will add.
	return newCourse.save().then((course, error) => {	//based on the schema/model, the price the name, description and price is what we will save as a new object.
		if(error) {
			return false
		} else {
			return true
		}
	});
}

//s39 Get/Retrieve all courses
module.exports.getAllCourses = () => { 	//getAllCourses is a function name.
	return Course.find({}).then(result => { //we will just retrieve so on postman just press send, yun lang.
		return result;
	})
}

//s39 Controller for retrieving all ACTIVE courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

//s39 Retrieving a specific course we will use ID here
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

//s39 Updating a course
// module.exports.updateCourse = (reqParams, reqBody, data) => {
// 	if(data.isAdmin === true) {				//sa post
// 		let updateCourse = {
// 			name: reqBody.name,
// 			description: reqBody.description,
// 			price: reqBody.price
// 		};
// 		return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	} else {
// 		return false //false if not an admin
// 	}
// }

//40
module.exports.updateCourse = (reqParams, reqBody, data) => {

    return User.findById(data.id).then(result => {
        console.log(result)

        if(result.isAdmin === true) {

            // Specify the fields/properties of the document to be updated
            let updatedCourse = {
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            };

            // Syntax:
                        // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

                // Course not updated
                if(error) {

                    return false

                // Course updated successfully    
                } else {

                    return true
                }
            })

        } else {

            return "user not admin"
        }



    })


}
//=========================

