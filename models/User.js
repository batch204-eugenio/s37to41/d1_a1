const mongoose = require("mongoose");
const UserSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "email is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false	
		//39start==========
		
		//39end============
	}, password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	}, 
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required"]
			},
			enrolledOn: {
    			type: Date,
    			default: new Date()
    		},
    		status: {
    			type: String,
    		}
		}
	]
})

module.exports = mongoose.model("User", UserSchema);
//in the activity if we want the user to become an Admin, we need manually update it to the database MONGODB.
//we are the ones who will set the admin.